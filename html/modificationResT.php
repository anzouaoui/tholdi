<?php
include '../html/entete.php';
include_once '../php/_gestionBase.inc.php';
?>
<html lang="fr">
    <head>
        <link rel="stylesheet" href="../css/coResT.css">
        <link rel="stylesheet" href="../bootstrap-3.3.4-dist/css/datepicker.css">
        <link rel="stylesheet" href="../bootstrap-3.3.4-dist/css/bootstrap.css">
        <link rel="stylesheet" href="../css/jquery-ui.css">
        <link rel="stylesheet" href="../css/jquery-ui.theme.css">
    </head>
    <body>
        <div class="container">
            <form role="form" role="form" method="post" action="../php/modification.traitement.php">
                <div class="col-xs-12 col-md-12 col-lg-12 col-sm-12 ">
                    <div class="col-md-12 col-xs-12 col-lg-12 col-sm-12 ">
                        <h3>Modification de votre reservation</h3>
                        <div class="well-lg">
                            <div class="container">
                                <?php
                                $collectionReservation = obtenirReservation();
                                if ($collectionReservation != false):
                                    foreach ($collectionReservation as $reservationCourante):
                                        ?>
                                        <div class="row">
                                            <div class="container">
                                                <div class='col-xs-12 col-md-5'>

                                                    <div class="form-group">
                                                        <label for="dateDebutReservation">du</label>
                                                        <div class='input-group date' class="date-picker">
                                                            <input type='date' class="form-control" value="<?php echo $reservationCourante["dateDebutReservation"]; ?>"name="dateDebutReservation"/>
                                                            <span class="input-group-addon">
                                                                <span class="glyphicon glyphicon-calendar"></span>
                                                            </span>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="container">
                                                <div class='col-xs-12 col-md-5'>
                                                    <div class="form-group">
                                                        <label for="dateFinReservation">au</label>
                                                        <div class='input-group date' class="date-picker">
                                                            <input type='date' class="form-control" value="<?php echo $reservationCourante["dateFinReservation"] ?>"name="dateFinReservation" />
                                                            <span class="input-group-addon">
                                                                <span class="glyphicon glyphicon-calendar"></span>
                                                            </span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <br />
                                        <br />
                                        <div class="row">
                                            <div class="container">
                                                <div class="col-xs-12 col-md-5">
                                                    <div class="form-group">
                                                        <label class="control-label" for="volumeEstime">Volume estimé:</label>
                                                        <input type="number" class="form-control" id="adresse" value="<?php echo $reservationCourante["volumeEstime"]; ?>" required name="volumeEstime">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    <?php endforeach; ?>
                                <?php endif; ?>
                                <br />
                                <br />
                                <div class="form-group">
                                    <label class="control-label col-sm-2" for="codeVilleMiseDispo">Port de chargement:</label>
                                    <div class="col-xs-12 col-sm-5">
                                        <select class="form-control" name="codeVilleMiseDispo" >
                                            <?php $collectionVilleMiseDispo = obtenirVille();
                                            ?>

                                            <?php foreach ($collectionVilleMiseDispo as $ville): ?>

                                                <option   value="<?php echo $ville["codeVille"]; ?>"><?php echo $ville["nomVille"]; ?></option>
                                            <?php endforeach; ?>
                                        </select>
                                    </div>
                                </div>
                                <br />
                                <br />
                                <div class="form-group" >
                                    <label class="control-label col-sm-2" for="codeVilleRendre">Port de déchargement:</label>
                                    <div class="col-xs-12 col-sm-5">
                                        <select class="form-control" name="codeVilleRendre">
                                            <?php $collectionVilleRendre = obtenirVille(); ?>

                                            <?php foreach ($collectionVilleRendre as $ville): ?>
                                                <option  value="<?php echo $ville["codeVille"]; ?>"><?php echo $ville["nomVille"]; ?></option>
                                            <?php endforeach; ?>
                                        </select>
                                    </div>
                                </div>
                                <br />
                                <br />
                                <br />
                                <br />
                                <table class="choix_rangees">
                                    <tr>
                                        <th class="entete">
                                            <label>CONTENEUR</label>
                                        </th>
                                        <th class="entete">
                                            <label>DIMENSION</label>
                                        </th>
                                        <th class="entete">
                                            <label>QUANTITE</label>
                                        </th>
                                        <th class="entete">
                                            <label>PRIX</label>
                                        </th>
                                    </tr>
                                    <?php
                                    $listeReservation = afficherConteneur();
                                    if ($listeReservation != false):
                                        foreach ($listeReservation as $reservationCourant):
                                            ?>
                                            <tr class="pas_coche" id="rangeenoi">
                                                <td>
                                                    <label for="typeContainer">
                                                        <input value="<?php echo $reservationCourant["libelleTypeContainer"]; ?>" readonly="readonly" style="border: none;" name="typeContainer[]">
                                                    </label>
                                                </td>
                                                <td>
                                                    <em>Longueur:</em> <?php echo $reservationCourant["longueurCont"]; ?>mm - <em>Largeur:</em> <?php echo $reservationCourant["largeurCont"]; ?>mm - <em>Hauteur:</em> <?php echo $reservationCourant["hauteurCont"]; ?>mm - <em>Capacite:</em><?php echo $reservationCourant["capaciteDeCharge"]; ?>CU.M
                                                </td>
                                                <td>
                                                    <input type="number" class="form-control" id="qteReserver" value="<?php echo $reservationCourant["qteReserver"]; ?>" required name="qteReserver[]">
                                                </td>
                                                <td>
                                                    <?php echo $reservationCourant["prix"] + "€"; ?> €
                                                </td>
                                            </tr>
                                        <?php endforeach; ?>
                                    <?php endif; ?>
                                </table>
                                <br />
                                <br />
                                <br />
                                <br />
                                <div class="col-xs-1 col-xs-offset-9">
                                    <button class="btn btn-primary nextBtn btn-lg" type="submit" >Modifier</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </body>