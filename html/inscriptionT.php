<!DOCTYPE html>
<?php
include_once '../html/entete.php';
include_once '../php/_gestionBase.inc.php';
?>

<section class="main container">
    <div class="miga-de-pan">
        <ol class="breadcrumb">
            <li><a href="../html/acceuilT.php">Page d'acceuil</a></li>
            <li><a href="../html/aProposT.php">&Agrave; propos</a></li>
            <li><a href="../html/conteneursT.php">Conteneurs</a></li>
        </ol>
    </div>

    <div class="row">
        <section class="posts col-md-9">
            <div class="miga-de-pan">
                <ol class="breadcrumb">
                    <li class="active">Inscription</li>
                </ol>
            </div>

            <article class="post clearfix">
                <p class="post-contenido text-justify">
                    <!-- texte -->
                <form class="form-horizontal" role="form" method="post" action="../php/inscription.traitement.php">
                    <div class="form-group">
                        <label class="control-label col-sm-2" for="raisonSociale">Raison Sociale:</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="raisonSociale" placeholder="action contre la fin " name="raisonSociale" required>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-sm-2" for="adresse">Adresse:</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="adresse" placeholder="Place d'Armes " required name="adresse">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-sm-2" for="cp">Code postal:</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="cp" placeholder="78000" name="cp" required="">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-sm-2" for="ville">Ville:</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="ville" placeholder="Versailles" name="ville" required="">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-sm-2" for="adrMel">Email:</label>
                        <div class="col-sm-10">
                            <input type="email" class="form-control" id="adrMel" placeholder="acfversailles@example.com" pattern="^[a-z0-9._-]+@[a-z0-9._-]{2,}\.[a-z]{2,4}$" name="adrMel" required>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-sm-2" for="telephone">Téléphone:</label>
                        <div class="col-sm-10">
                            <input type="text" pattern="^0[1-9]([-. ]?[0-9]{2}){4}$" class="form-control" id="telephone" placeholder="01.23.45.67.89" name="telephone" required>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-sm-2" for="contact">Personne à contacter:</label>
                        <div class="col-sm-10">
                            <input type="text"  class="form-control" id="contact" placeholder="Louis XIV" name="contact" required>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-sm-2" for="codePays">Pays:</label>
                        <div class="col-sm-10">
                            <select name="codePays"class="form-control">
                                <?php $collectionPays = obtenirPays(); ?>

                                <?php foreach ($collectionPays as $pays): ?>

                                    <option  value="<?php echo $pays["codePays"]; ?>"><?php echo $pays["nomPays"]; ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-sm-2" for="mdp">Mot de passe:</label>
                        <div class="col-sm-10"> 
                            <input type="password" class="form-control" id="mdp" placeholder="mdp" name="mdp"  pattern=".{6,}" title="6 caractères minimum" required>
                        </div>
                    </div>

                    <div class="form-group"> 
                        <div class="col-sm-offset-2 col-sm-10">
                            <div class="checkbox">
                                <label><input type="checkbox" required> J'accepte les termes et les <a href="#">conditions</a> </label>
                            </div>
                        </div>
                    </div>
                        <div class="form-group"> 
                            <div class="col-sm-offset-2 col-sm-10">
                                <button type="submit" class="btn btn-default" >Envoyer</button>
                            </div>
                        </div>
                </form>
                <!-- fin texte-->
                </p>
            </article>
        </section>
        <aside class="col-md-3 hidden-xs hidden-sm">
            <h4>Catégorie</h4>
            <div class="list-group">
                <a href="../html/acceuilT.php" class="list-group-item">Page d'acceuil</a>
                <a href="../html/aProposT.php" class="list-group-item">&Agrave; propos</a>
                <a href="../html/conteneursT.php" class="list-group-item">Conteneurs</a>
            </div>

            <h4>Articles Récents</h4>
            <a href="http://www.atelier.net/trends/articles/fret-maritime-gerer-complexite-grace-software_439129" class="list-group-item">
                <h4 class="list-group-item-heading">Fret maritime : gérer la complexité grâce au software</h4>
                <p class="list-group-item-text"></p>
            </a>

            <a href="http://www.lepoint.fr/economie/transport-maritime-cma-cgm-veut-racheter-neptune-orient-lines-07-12-2015-1987771_28.php" class="list-group-item">
                <h4 class="list-group-item-heading">Transport maritime : CMA CGM veut racheter Neptune Orient Lines</h4>
                <p class="list-group-item-text"></p>
            </a>

            <a href="http://www.francetvinfo.fr/meteo/climat/cop21/cop21-pourquoi-l-accord-ne-suffira-pas-pour-sauver-la-planete_1221853.html" class="list-group-item">
                <h4 class="list-group-item-heading">COP21 : pourquoi l'accord ne suffira pas pour sauver la planète</h4>
                <p class="list-group-item-text"></p>
            </a>

            <a href="http://www.liberation.fr/planete/2015/12/10/les-negociateurs-reculent-face-aux-transports-aeriens-et-maritimes_1419759" class="list-group-item">
                <h4 class="list-group-item-heading">Les négociateurs reculent face aux transports aériens et maritimes</h4>
                <p class="list-group-item-text"></p>
            </a>

            <a href="http://meretmarine.com/fr/node/136695" class="list-group-item">
                <h4 class="list-group-item-heading">CMA CGM : Le plus gros porte-conteneurs accueilli aux USA</h4>
                <p class="list-group-item-text"></p>
            </a>
        </aside>
    </div>
</section>

<?php include_once '../html/piedPage.php'; ?>

<script src="../java/jquery.js"></script>
<script src="../bootstrap-3.3.4-dist/js/bootstrap.min.js"></script>
</body>
</html>