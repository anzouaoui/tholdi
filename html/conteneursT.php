<!DOCTYPE html>
<?php include '../html/entete.php'; ?>
<html lang="fr">
    <head>
        <link rel="stylesheet" href="../css/conteneursT.css">
    </head>
    <section class="main container">
        <div class="miga-de-pan">
            <ol class="breadcrumb">
                <li><a href="../html/acceuilT.php">Page d'acceuil</a></li>
                <li><a href="../html/aProposT.php">&Agrave; propos</a></li>
                <li><a href="../html/conteneurT.php">Conteneurs</a></li>
            </ol>
        </div>

        <div class="row">
            <section class="posts col-md-9">
                <div class="miga-de-pan">
                    <ol class="breadcrumb">
                        <li class="active">Conteneurs</li>
                    </ol>
                </div>

                <article class="post clearfix">
                    <p class="post-contenido text-justify">
                        Etant un leader mondial du transport maritime conteneurisé, THOLDI possède une multitude de flotte et de conteneurs, 
                        susceptibles de transporter tous types de marchandises. Que ça soit de la marchandise en vrac, sacs ou cartons dans
                        des conteneurs « standards », marchandises hors gabarit et project cargo sur des « flat racks », en passant par des 
                        fruits, viandes, poissons, ou toutes autres marchandises à température dirigée dans des conteneurs frigorifiques, 
                        THOLDI a toujours une réponse adaptée aux besoins de ses clients.
                    </p>
                    <div class="col-md-12 col-sm-9 col-xs-9" id="container">
                        <div class="row">
                            <h1>Les conteneurs</h1>
                            <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                                <div class="panel panel-custom">
                                    <div class="panel-heading" role="tab" id="headingOne">
                                        <h4 class="panel-title">
                                            <a data-toggle="collapse" data-parent="#accordion" href="#dry" aria-expanded="true" aria-controls="dry">
                                                <i class="glyphicon glyphicon-plus"></i>
                                                Dry
                                            </a>
                                        </h4>
                                    </div>
                                    <div id="dry" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                                        <div class="panel-body animated zoomOut">
                                            <img class="img-thumbnail" src="../image/conteneurdry.jpg" alt="conteneurdry" width="304" height="236"/>
                                            <p class="post-conten text-justify">
                                                Conteneurs tous usages <br>
                                                Un conteneur maritime totalement cloisonné et étanche aux intempéries, avec un toit, des parois latérales et un plancher rigides 
                                                comportant au moins une de ses parois équipée d’une porte et construit aux fins de transporter tous types de marchandises.
                                                <br>Il s’agit du conteneur le plus courant, capable de transporter la plupart des marchandises « sèches », boites, cartons, caisses, sacherie, 
                                                balles, palettes, futs, etc. Il est équipé d'anneaux de sécurité/saisissage.
                                                <br>Moyennant certains ajustements intérieurs et à condition de disposer des équipements nécessaires au chargement/déchargement, ce type de 
                                                conteneur peut être utilisé pour le transport de certains types de marchandises en vrac.
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="panel panel-custom">
                                    <div class="panel-heading" role="tab" id="headingTwo">
                                        <h4 class="panel-title">
                                            <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#Reefer" aria-expanded="false" aria-controls="Reefer">
                                                <i class="glyphicon glyphicon-plus"></i>
                                                Reefer
                                            </a>
                                        </h4>
                                    </div>
                                    <div id="Reefer" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                                        <div class="panel-body animated zoomOut">
                                            <img class="img-thumbnail" src="../image/conteneurreefer.jpg" alt="conteneurreefer" width="404" height="236"/>
                                            <p class="post-conten text-justify">
                                                Ce type de conteneur est utilisé pour le transport sous température dirigée. Avec isolation thermique, il est équipé d’un moteur électrique 
                                                (compresseur mécanique) afin de rafraichir ou de réchauffer l’atmosphère à l’intérieur du conteneur.
                                                <br>CMA CGM possède une des flottes les plus importantes et les plus modernes de conteneurs réfrigérés pour le transport de biens périssables dans un environnement 
                                                sous température dirigée (de – 35°c à + 30°c). La flotte de conteneurs réfrigérés CMA CGM est constituée de 20’, de 40’ High Cube et de 45’ 32/33 Pallet wide.
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="panel panel-custom">
                                    <div class="panel-heading" role="tab" id="headingThree">
                                        <h4 class="panel-title">
                                            <i class="glyphicon glyphicon-plus"></i>                    
                                            <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#OpenTop" aria-expanded="false" aria-controls="OpenTop">                            
                                                Open Top
                                            </a>                        
                                        </h4>
                                    </div>
                                    <div id="OpenTop" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                                        <div class="panel-body animated zoomOut">
                                            <img class="img-thumbnail" src="../image/conteneurOpenTo.jpg" alt="conteneurOpenTo" width="304" height="236"/>
                                            <p class="post-conten text-justify">
                                                Un conteneur maritime dont les caractéristiques sont identiques à celle d’un conteneur DRY sinon le fait qu’il est dépourvu d’un toit rigide et qu’il peut être 
                                                équipé d’une couverture mobile ou détachable et de poutres transversales au-dessus des portes.
                                                <br>Ces conteneurs sont spécialement conçus pour le transport de produits manufacturés lours et / ou volumineux dont la manutention et le chargement ne peuvent être effectués qu’avec 
                                                l’aide d’une grue ou d’un pont roulant.
                                                <br>Des conteneurs OPEN TOP demi hauteur basculant sont spécialement conçus pour le transport de minerais en vrac. 
                                                <br>CMA CGM exploite des conteneurs OPEN TOP de 8’6’’ équipés :
                                                <br>D’arceaux amovibles et d’une bâche
                                                <br>De portes montées sur gonds et /ou d’une poutre transversale amovible au-dessus des portes.
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="panel panel-custom">
                                    <div class="panel-heading" role="tab" id="headingFour">
                                        <h4 class="panel-title">
                                            <i class="glyphicon glyphicon-plus"></i>                    
                                            <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#FlatRack" aria-expanded="false" aria-controls="FlatRack">                            
                                                Flat Rack
                                            </a>                        
                                        </h4>
                                    </div>
                                    <div id="FlatRack" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                                        <div class="panel-body animated zoomOut">
                                            <img class="img-thumbnail" src="../image/conteneurflatrack.jpg" alt="conteneurflatrack" width="304" height="236"/>
                                            <p class="post-conten text-justify">
                                                Les conteneurs Flat Racks se présentent avec ou sans parois rigides aux deux extrémités (parois qui peuvent être rabattues sur le plancher).
                                                <br>Les FLAT RACKS ont pour objectif le transport de marchandises volumineuses, lourdes ou hors gabarit (en hauteur ou en largeur). Les Flat Racks dont les deux extrémités sont 
                                                rabattables permettent aussi le chargement de cargaisons en extra longueur. Leur plancher a été conçu pour le transport de colis lourds, (jusqu’à 45 T pour certains).
                                                <br>Ils permettent, à vide, le chargement en pile qui facilite le repositionnement des vides.
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="panel panel-custom">
                                    <div class="panel-heading" role="tab" id="headingFive">
                                        <h4 class="panel-title">
                                            <i class="glyphicon glyphicon-plus"></i>                    
                                            <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#Citerne" aria-expanded="false" aria-controls="Citerne">                            
                                                Citerne
                                            </a>                        
                                        </h4>
                                    </div>
                                    <div id="Citerne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                                        <div class="panel-body animated zoomOut">
                                            <img class="img-thumbnail" src="../image/conteneurciterne.jpg" alt="conteneurciterne" width="304" height="236"/>
                                            <p class="post-conten text-justify">
                                                Un conteneur maritime composé de deux éléments, la citerne et le cadre. Ce type de conteneur CITERNE est utilisé pour le transport de 
                                                liquide dangereux ou non, y compris les produits alimentaires.
                                                <br>Ils sont équipés des accessoires destinés à faciliter le chargement et le déchargement du contenu et sont aussi équipés d’accessoires de sécurité.
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="panel panel-custom">
                                    <div class="panel-heading" role="tab" id="headingSix">
                                        <h4 class="panel-title">
                                            <i class="glyphicon glyphicon-plus"></i>                    
                                            <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#HighCube" aria-expanded="false" aria-controls="HighCube">                            
                                                High Cube Palletwide
                                            </a>                        
                                        </h4>
                                    </div>
                                    <div id="HighCube" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                                        <div class="panel-body animated zoomOut">
                                            <img class="img-thumbnail" src="../image/conteneurHighCube.jpg" alt="conteneurHighCube" width="304" height="236"/>
                                            <p class="post-conten text-justify">
                                                Les conteneurs 40’/45’ PALLET WIDE ont été spécialement conçus pour le transport de palettes de 120 cm aux normes européennes.
                                                <br>Cet équipement avec des dimensions intérieures de 2,45 m permet d’optimiser l’utilisation du volume de chargement. Les chargeurs bénéficient du fait de pouvoir charger 5 
                                                palettes de 1,20m x 0,80m supplémentaires par rapport à un 45’ standard. La capacité totale par rangée est de 33 « euro palettes » dans un 45’ « PALLET WIDE" contre 27 « Euro palettes » 
                                                dans un 45’ standard (ou 24 x "1,20m x 1 m" contre 21). Ces conteneurs sont conformes aux normes Européennes.
                                                <br>Cette capacité supplémentaire offre au chargeur la capacité de réduire les couts de distribution de 15% ou plus. Cette utilisation optimale de l’espace, sans chargements fractionnés, 
                                                permet d’éviter les mouvements de marchandise.
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </article>
            </section>
            <aside class="col-md-3 hidden-xs hidden-sm">
                <h4>Catégorie</h4>
                <div class="list-group">
                    <a href="../html/acceuilT.php" class="list-group-item">Page d'acceuil</a>
                    <a href="../html/aProposT.php" class="list-group-item">&Agrave; propos</a>
                    <a href="../html/conteneursT.php" class="list-group-item active">Conteneurs</a>
                </div>

                <h4>Articles Récents</h4>
                <a href="http://www.atelier.net/trends/articles/fret-maritime-gerer-complexite-grace-software_439129" class="list-group-item">
                    <h4 class="list-group-item-heading">Fret maritime : gérer la complexité grâce au software</h4>
                    <p class="list-group-item-text"></p>
                </a>

                <a href="http://www.lepoint.fr/economie/transport-maritime-cma-cgm-veut-racheter-neptune-orient-lines-07-12-2015-1987771_28.php" class="list-group-item">
                    <h4 class="list-group-item-heading">Transport maritime : CMA CGM veut racheter Neptune Orient Lines</h4>
                    <p class="list-group-item-text"></p>
                </a>

                <a href="http://www.francetvinfo.fr/meteo/climat/cop21/cop21-pourquoi-l-accord-ne-suffira-pas-pour-sauver-la-planete_1221853.html" class="list-group-item">
                    <h4 class="list-group-item-heading">COP21 : pourquoi l'accord ne suffira pas pour sauver la planète</h4>
                    <p class="list-group-item-text"></p>
                </a>

                <a href="http://www.liberation.fr/planete/2015/12/10/les-negociateurs-reculent-face-aux-transports-aeriens-et-maritimes_1419759" class="list-group-item">
                    <h4 class="list-group-item-heading">Les négociateurs reculent face aux transports aériens et maritimes</h4>
                    <p class="list-group-item-text"></p>
                </a>

                <a href="http://meretmarine.com/fr/node/136695" class="list-group-item">
                    <h4 class="list-group-item-heading">CMA CGM : Le plus gros porte-conteneurs accueilli aux USA</h4>
                    <p class="list-group-item-text"></p>
                </a>
            </aside>
        </div>
    </section>
    <?php include_once '../html/piedPage.php'; ?>

<script src="../java/jquery.js"></script>
<script src="../bootstrap-3.3.4-dist/js/bootstrap.js" type="text/javascript"></script>
<script src="../java/conteneursT.js"></script>
</body>
</html>