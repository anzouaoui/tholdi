<!DOCTYPE html>
<?php
include_once '../php/_gestionBase.inc.php';
?>
<html lang="fr">
    <head>
        <meta charset ="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
        <title>THOLDI</title>
        <link rel="icon" type="image/x-icon" href="../image/minilogo.jpg">
        <link href="../bootstrap-3.3.4-dist/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
        <link href="../css/styleBaseT.css" rel="stylesheet" type="text/css"/>
        <link rel="stylesheet" href="../bootstrap-datetimepicker/css/bootstrap-datetimepicker.css">
        <script src="../java/jquery.js" type="text/javascript"></script>
        <script src="../bootstrap-3.3.4-dist/js/bootstrap.js" type="text/javascript"></script>
        <script type="text/javascript" src="../bootstrap-datetimepicker/js/bootstrap-datetimepicker.js"></script>
        <script type="text/javascript" src="../java/coRest2.js"></script>
    </head>
    <body>
        <header>
            <nav class="navbar navbar-inverse navbar-static-top" role="navigation">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-2 col-xs-2 col-md-2 col-sm-2">
                            <a href="../html/acceuilT.php" class="navbar-brand">
                                <img src="../image/minilogo.jpg" id="logo">
                            </a>
                        </div>
                        <div class="col-xs-10 col-lg-10 col-sm-10 col-md-10">
                            <div class="navbar-header">
                                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navegacion-fm">
                                    <span class="sr-only">Desplegar / Ocultar Menu</span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                </button>

                                <!-- Inicia Menu http://www.falconmasters.com/cursos/curso-sitio-web-bootstrap/ 11:28/2-->
                                <div class="collapse navbar-collapse" id="navegacion-fm">
                                    <ul class="nav navbar-nav" id="nav">
                                        <li class="active"><a href="../html/acceuilT.php">Acceuil</a></li>
                                        <li><a href="../html/galleryT.php">Galerie</a></li>
                                        <li><a href="../html/coResT.php">Réservation</a></li>
                                        <li><a href="../html/acceuilTen.php">En</a></li>
                                    </ul>
                                     &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                                    <?php if (!isset($_SESSION["adrMel"])): ?>
                                        <form  method="post" class="navbar-form navbar-right" role="form" action="../html/acceuilT.php">
                                            <div class="form-group">
                                                <input name="adrMel" type="text" placeholder="mail" class="form-control">
                                            </div>
                                            <div class="form-group">
                                                <input name="mdp" type="password" placeholder="mot de passe" class="form-control">
                                            </div>
                                            <button type="submit" class="btn btn-success">S'authentifier</button>
                                        </form>
                                    <?php else: ?>
                                      &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                                            <span class="glyphicon glyphicon-user white" aria-hidden="true" ></span>
                                            &nbsp;&nbsp;
                                            <span class="text-center white"><?php echo $_SESSION["raisonSociale"]; ?></span>
                                             &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                                            <a href="<?php echo $_SERVER["PHP_SELF"]; ?>?logout" class="white">
                                                <span class="glyphicon glyphicon-log-out " aria-hidden="true" title="log-out"></span>
                                            </a>
                                    <?php endif; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </nav>
        </header>

        <section class="jumbotron">
            <div class="container">
                <h1>&Agrave; chacun sa marchandise</h1>
                <p>Tholdi assure le transport de votre cargaison !</p>
            </div>
        </section>
