<!DOCTYPE html>
<?php include_once '../html/header.php'; ?>
<section class="main container">
    <div class="miga-de-pan">
        <ol class="breadcrumb">
            <li><a href="../html/acceuilTen.php">Home page</a></li>
            <li><a href="../html/aProposTen.php">About</a></li>
            <li><a href="../html/conteneursTen.php">containers</a></li>
        </ol>
    </div>

    <div class="row">
        <section class="posts col-md-9">
            <div class="miga-de-pan">
                <ol class="breadcrumb">
                    <li class="active">Registration</li>
                </ol>
            </div>

            <article class="post clearfix">
                <p class="post-contenido text-justify">
                    <!-- texte -->
                <form class="form-horizontal" role="form">

                    <div class="form-group">
                        <label class="control-label col-sm-2" for="email">Name:</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="nom" placeholder="Doe" required>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-sm-2" for="email">First name:</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="prenom" placeholder="Jane" required>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-sm-2" for="email">Phone:</label>
                        <div class="col-sm-10">
                            <input type="text" pattern="^0[1-7]([-. ]?[0-9]{2}){4}$" class="form-control" id="tel" placeholder="01.23.45.67.89" required>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-sm-2" for="email">E-mail:</label>
                        <div class="col-sm-10">
                            <input type="email" class="form-control" id="email" placeholder="jane.doe@example.com" pattern="^[a-z0-9._-]+@[a-z0-9._-]{2,}\.[a-z]{2,4}$" required>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-sm-2" for="email">Country:</label>
                        <div class="col-sm-10">
                            <select class="form-control">
                                <option>France</option>
                                <option>Britain</option>
                                <option>Germany</option>
                                <option>Spain</option>
                                <option>Italy</option>
                                <option>China</option>
                                <option>Africa</option>
                                <option>United States</option>
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-sm-2" for="email">Postal code:</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="email" placeholder="78700">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-sm-2" for="pwd">Password:</label>
                        <div class="col-sm-10"> 
                            <input type="password" class="form-control" id="pwd" placeholder="mdp" required>
                        </div>
                    </div>

                    <div class="form-group"> 
                        <div class="col-sm-offset-2 col-sm-10">
                            <label class="radio-inline">
                                <input type="radio" name="inlineRadioOptions" id="inlineRadio1" value="homme"> Man
                            </label>
                            <label class="radio-inline">
                                <input type="radio" name="inlineRadioOptions" id="inlineRadio2" value="femme"> Woman
                            </label>
                        </div>
                    </div>

                    <div class="form-group"> 
                        <div class="col-sm-offset-2 col-sm-10">
                            <label class="radio-inline">
                                <input type="radio" name="statut" id="particulier" value="particulier"> Individuals
                            </label>
                            <label class="radio-inline">
                                <input type="radio" name="statut" id="pro" value="pro"> Businesses
                            </label>
                        </div>
                    </div>

                    <div class="form-group"> 
                        <div class="col-sm-offset-2 col-sm-10">
                            <div class="checkbox">
                                <label><input type="checkbox" required> I accept the terms and conditions </label>
                            </div>
                        </div>
                    </div>
                    <a href="../html/coResTen.php">
                        <div class="form-group"> 
                            <div class="col-sm-offset-2 col-sm-10">
                                <button type="submit" class="btn btn-default">Send</button>
                            </div>
                        </div>
                    </a>
                </form>
                <!-- fin texte-->
                </p>
            </article>
        </section>
        <aside class="col-md-3 hidden-xs hidden-sm">
            <h4>Category</h4>
            <div class="list-group">
                <a href="../html/acceuilTen.php" class="list-group-item">Home page</a>
                <a href="../html/aProposTen.php" class="list-group-item">About</a>
                <a href="../html/conteneursTen.php" class="list-group-item">Containers</a>
            </div>

            <h4>Articles Récents</h4>
            <a href="http://www.atelier.net/trends/articles/fret-maritime-gerer-complexite-grace-software_439129" class="list-group-item">
                <h4 class="list-group-item-heading">Fret maritime : gérer la complexité grâce au software</h4>
                <p class="list-group-item-text"></p>
            </a>

            <a href="http://www.lepoint.fr/economie/transport-maritime-cma-cgm-veut-racheter-neptune-orient-lines-07-12-2015-1987771_28.php" class="list-group-item">
                <h4 class="list-group-item-heading">Transport maritime : CMA CGM veut racheter Neptune Orient Lines</h4>
                <p class="list-group-item-text"></p>
            </a>

            <a href="http://www.francetvinfo.fr/meteo/climat/cop21/cop21-pourquoi-l-accord-ne-suffira-pas-pour-sauver-la-planete_1221853.html" class="list-group-item">
                <h4 class="list-group-item-heading">COP21 : pourquoi l'accord ne suffira pas pour sauver la planète</h4>
                <p class="list-group-item-text"></p>
            </a>

            <a href="http://www.liberation.fr/planete/2015/12/10/les-negociateurs-reculent-face-aux-transports-aeriens-et-maritimes_1419759" class="list-group-item">
                <h4 class="list-group-item-heading">Les négociateurs reculent face aux transports aériens et maritimes</h4>
                <p class="list-group-item-text"></p>
            </a>

            <a href="http://meretmarine.com/fr/node/136695" class="list-group-item">
                <h4 class="list-group-item-heading">CMA CGM : Le plus gros porte-conteneurs accueilli aux USA</h4>
                <p class="list-group-item-text"></p>
            </a>
        </aside>
    </div>
</section>

<?php include_once '../html/footer.php'; ?>

<script src="../java/jquery.js"></script>
<script src="../bootstrap-3.3.4-dist/js/bootstrap.min.js"></script>
</body>
</html>