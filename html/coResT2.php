<!DOCTYPE html>
<?php
include '../html/entete.php';
include_once '../php/_gestionBase.inc.php';
?>
<html lang="fr">
    <head>
        <link rel="stylesheet" href="../css/coResT.css">
        <link rel="stylesheet" href="../bootstrap-3.3.4-dist/css/datepicker.css"> 
        <link rel="stylesheet" href="../bootstrap-3.3.4-dist/css/bootstrap.css">
    </head>
    <!-- Début -->
    <div class="container">
        <div class="stepwizard">
            <div class="stepwizard-row setup-panel">
                <div class="stepwizard-step">
                    <a href="../html/coResT.php" type="button" class="btn btn-primary btn-circle" disabled="disabled">1</a>
                    <p>&Eacute;tape 1</p>
                </div>
                <div class="stepwizard-step">
                    <a href="../html/coResT2.php" type="button" class="btn btn-default btn-circle active" >2</a>
                    <p>&Eacute;tape 2</p>
                </div>
                <div class="stepwizard-step">
                    <a href="../html/coResT3.php" type="button" class="btn btn-default btn-circle" disabled="disabled">3</a>
                    <p>&Eacute;tape 3</p>
                </div>
                <div class="stepwizard-step">
                    <a href="../html/devis.php" type="button" class="btn btn-default btn-circle" disabled="disabled">4</a>
                    <p>&Eacute;tape 4</p>
                </div>
            </div>
        </div>
        <!--Formulaire de reservation-->
        <form role="form" method="post" action="../php/reserver.traitement.php">
            <div class="row">
                <div class="col-sm-10">
                    <h3>Conteneurs</h3>
                    <section id="reservationContainer">
                        <div class="form-group">
                            <label class="control-label col-sm-2" for="typeContainer">Types: </label>
                            <div class="col-sm-10">
                                <select class="form-control" name="typeContainer">
                                    <?php $collectionTypeContainer = obtenirConteneur(); ?>

                                    <?php foreach ($collectionTypeContainer as $typeContainer): ?>
                                        <option  value="<?php echo $typeContainer["typeContainer"]; ?>"><?php echo $typeContainer["libelleTypeContainer"]; ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                            <br />
                            <br />
                            <br />
                            <br />
                            <div class="form-group">
                                <label class="control-label col-sm-2" for="qteReserver">quantité:</label>
                                <div class="col-sm-2">
                                    <input type="number" class="form-control" id="adresse" placeholder="1 " required name="qteReserver">
                                </div>
                            </div>
                            <br />
                            <br />
                            <br />
                            <button class="btn btn-primary nextBtn btn-lg pull-right" type="submit" >Valider</button>
                    </section>
                </div>
            </div>
                <!-- Affichage des conteneurs réserver-->
            <table class="choix_rangees">
                <tr>
                    <th>
                        <label class="entete">CONTENEUR</label>
                    </th>
                    <th>
                        <label class="entete">DIMENSION</label>
                    </th>
                    <th>
                        <label class="entete">QUANTITE</label>
                    </th><th>
                        <label class="entete">PRIX</label>
                    </th>
                </tr>
                <?php 
                $listeReservation = afficherConteneur();
                if($listeReservation != false):
                    foreach($listeReservation as $reservationCourant):
                ?>
                <tr class="tableau">
                    <td id="type">
                        <?php echo $reservationCourant["libelleTypeContainer"] ; ?>
                    </td>
                    <td id="dimension">
                        <em>Longueur:</em> <?php echo $reservationCourant["longueurCont"]; ?>mm - <em>Largeur:</em> <?php echo $reservationCourant["largeurCont"]; ?>mm - <em>Hauteur:</em> <?php echo $reservationCourant["hauteurCont"]; ?>mm - <em>Capacite:</em><?php echo $reservationCourant["capaciteDeCharge"]; ?>CU.M
                    </td>
                    <td id="quantite">
                        <?php echo $reservationCourant["qteReserver"]; ?>
                    </td>
                    <td id="prix">
                        <?php echo $reservationCourant["prix"] + "€"; ?> €
                    </td>
                </tr>
                <?php endforeach; ?>
                <?php endif; ?>
            </table>
            <div>
                <a href="../html/coResT3.php">
                    <button class="btn btn-primary nextBtn btn-lg pull-right" type="button" >Suite</button>
                </a>
            </div>
        </form>
    </div>
    <?php include_once '../html/piedPage.php'; ?>

    <script src="../java/jquery.js"></script>
    <script src="../bootstrap-3.3.4-dist/js/bootstrap.min.js"></script>
    <script src="../java/coResT.js"></script>
    <script src="../java/coResT2.js"></script>
    <script src="../java/recapitulatif.js"></script>
    <script src="../jquery/main.js"></script>
    <script src="../bootstrap-3.3.4-dist/js/bootstrap.js"></script>
    <!--<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.js"></script>-->
</body>
</html>
<!--
http://bootsnipp.com/snippets/featured/form-wizard-and-validation
http://bootsnipp.com/snippets/featured/float-label-pattern-forms
http://bootsnipp.com/snippets/3kDmD
-->