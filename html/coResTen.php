<?php include '../html/header.php'; ?>
<html lang="fr">
    <head>
        <link rel="stylesheet" href="../css/coResT.css">
    </head>
    <section class="main container">
        <div class="miga-de-pan">
            <ol class="breadcrumb">
                <li><a href="../html/acceuilTen.php">Home page</a></li>
                <li><a href="../html/aProposTen.php">About</a></li>
                <li><a href="../html/conteneursTen.php">containers</a></li>
                <li><a href="../html/inscriptionTen.php">Registration</a></li>
            </ol>
        </div>

        <div class="container">
            <div class="stepwizard">
                <div class="stepwizard-row setup-panel">
                    <div class="stepwizard-step">
                        <a href="#step-1" type="button" class="btn btn-primary btn-circle">1</a>
                        <p>Stage 1</p>
                    </div>
                    <div class="stepwizard-step">
                        <a href="#step-2" type="button" class="btn btn-default btn-circle" disabled="disabled">2</a>
                        <p>Stage 2</p>
                    </div>
                    <div class="stepwizard-step">
                        <a href="#step-3" type="button" class="btn btn-default btn-circle" disabled="disabled">3</a>
                        <p>Stage 3</p>
                    </div>
                </div>
            </div>

            <form role="form">
                <div class="row setup-content" id="step-1">
                    <div class="col-xs-12">
                        <div class="col-md-12 well">
                            <h3>Connection</h3>
                            <div class="form-group">
                                <label class="control-label col-sm-3" for="email">Name :</label><br>
                                <div class="col-sm-12">
                                    <input type="text" class="form-control" id="nom" placeholder="Doe"><br>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-sm-3" for="pwd">Password:</label><br>
                                <div class="col-sm-12"> 
                                    <input type="password" class="form-control" id="pwd" placeholder="mdp" ><br>
                                </div>
                            </div>
                            <div class="col-xs-3 col-xs-offset-9">            
                                <button class="btn btn-primary nextBtn btn-lg pull-right" type="button">Next</button>
                            </div>
                            <div class="col-xs-3 col-xs-offset-9 inscription">
                                <ul class="list-inline text-right">
                                    <li><a href="../html/inscriptionTen.php">Subscribe</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row setup-content" id="step-2">
                    <div class="col-xs-12">
                        <div class="col-md-12">
                            <h3>Selection</h3>
                            <div class="container">
                                <p>Choice of container</p>
                                <button class="btn btn-primary nextBtn btn-lg pull-right" type="button" >Next</button>
                            </div>
                        </div>
                    </div>
                    <div class="row setup-content" id="step-3">
                        <div class="col-xs-12">
                            <div class="col-md-12">
                                <h3>Summary</h3>
                                <div class="container">

                                </div>
                                <button class="btn btn-success btn-lg pull-right" type="submit">Validate</button>
                            </div>
                        </div>
                    </div>
            </form>
        </div>
        <?php include_once '../html/piedPage.php'; ?>

        <script src="../java/jquery.js"></script>
        <script src="../bootstrap-3.3.4-dist/js/bootstrap.min.js"></script>
        <script src="../java/coResT.js"></script>
        <script src="../java/coResT2.js"></script>
        <!--<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.js"></script>-->
    </body>
</html>
<!--
http://bootsnipp.com/snippets/featured/form-wizard-and-validation
http://bootsnipp.com/snippets/featured/float-label-pattern-forms
http://bootsnipp.com/snippets/3kDmD
-->

