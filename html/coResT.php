<!DOCTYPE html>
<?php
include '../html/entete.php';
include_once '../php/_gestionBase.inc.php';

?>
<html lang="fr">
    <head>
        <link rel="stylesheet" href="../css/coResT.css">
        <link rel="stylesheet" href="../bootstrap-3.3.4-dist/css/datepicker.css">
        <link rel="stylesheet" href="../bootstrap-3.3.4-dist/css/bootstrap.css">
        <link rel="stylesheet" href="../css/jquery-ui.css">
        <link rel="stylesheet" href="../css/jquery-ui.theme.css">

    </head>
    <!-- Début -->
    <body>
        <div class="container">
            <div class="stepwizard">
                <div class="stepwizard-row setup-panel">
                    <div class="stepwizard-step">
                        <a href="../html/coResT.php" type="button" class="btn btn-primary btn-circle active">1</a>
                        <p>&Eacute;tape 1</p>
                    </div>
                    <div class="stepwizard-step">
                        <a href="../html/coResT2.php" type="button" class="btn btn-default btn-circle" disabled="disabled">2</a>
                        <p>&Eacute;tape 2</p>
                    </div>
                    <div class="stepwizard-step">
                        <a href="../html/coResT3.php" type="button" class="btn btn-default btn-circle" disabled="disabled">3</a>
                        <p>&Eacute;tape 3</p>
                    </div>
                    <div class="stepwizard-step">
                        <a href="../html/devis.php" type="button" class="btn btn-default btn-circle" disabled="disabled">4</a>
                        <p>&Eacute;tape 4</p>
                    </div>
                </div>
            </div>
            <form role="form" role="form" method="post" action="../php/reservation.traitement.php">
                <div class="col-xs-12 col-md-12 col-lg-12 col-sm-12 ">
                    <div class="col-md-12 col-xs-12 col-lg-12 col-sm-12 ">
                        <h3>Réservation</h3>
                        <div class="well-lg">
                            <div class="container">
                                <div class="row">
                                    <div class="container">
                                        <div class='col-xs-12 col-md-5'>
                                            <div class="form-group">
                                                <label for="dateDebutReservation">du</label>
                                                <div class='input-group date' class="date-picker">
                                                    <input type='date' class="form-control" name="dateDebutReservation"/>
                                                    <span class="input-group-addon">
                                                        <span class="glyphicon glyphicon-calendar"></span>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="container">
                                        <div class='col-xs-12 col-md-5'>
                                            <div class="form-group">
                                                <label for="dateFinReservation">au</label>
                                                <div class='input-group date' class="date-picker">
                                                    <input type='date' class="form-control" name="dateFinReservation" />
                                                    <span class="input-group-addon">
                                                        <span class="glyphicon glyphicon-calendar"></span>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <br />
                                <br />
                                <div class="row">
                                  <div class="container">
                                    <div class="col-xs-12 col-md-5">
                                      <div class="form-group">
                                          <label class="control-label" for="volumeEstime">Volume estimé:</label>
                                          <input type="number" class="form-control" id="adresse" placeholder="1 " required name="volumeEstime">
                                      </div>
                                    </div>
                                  </div>
                                </div>
                                <br />
                                <br />
                                <div class="form-group">
                                    <label class="control-label col-sm-2" for="codeVilleMiseDispo">Port de chargement:</label>
                                    <div class="col-xs-12 col-sm-5">
                                        <select class="form-control" name="codeVilleMiseDispo" >
                                            <?php $collectionVilleMiseDispo = obtenirVille(); ?>

                                            <?php foreach ($collectionVilleMiseDispo as $ville): ?>

                                                <option   value="<?php echo $ville["codeVille"]; ?>"><?php echo $ville["nomVille"]; ?></option>
                                            <?php endforeach; ?>
                                        </select>
                                    </div>
                                </div>
                                <br />
                                <br />
                                <div class="form-group" >
                                    <label class="control-label col-sm-2" for="codeVilleRendre">Port de déchargement:</label>
                                    <div class="col-xs-12 col-sm-5">
                                        <select class="form-control" name="codeVilleRendre">
                                            <?php $collectionVilleRendre = obtenirVille(); ?>

                                            <?php foreach ($collectionVilleRendre as $ville): ?>
                                                <option  value="<?php echo $ville["codeVille"]; ?>"><?php echo $ville["nomVille"]; ?></option>
                                            <?php endforeach; ?>
                                        </select>
                                    </div>
                                </div>
                                <br />
                                <br />

                                <br />
                                <br />
                                <div class="col-xs-1 col-xs-offset-9">
                                    <button class="btn btn-primary nextBtn btn-lg" type="submit" >Suite</button>
                                </div>
                            </div>
                        </div>
                    </div>
            </form>
        </div>
        <?php include_once '../html/piedPage.php'; ?>
        <!--<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.js"></script>-->
    </body>
</html>
