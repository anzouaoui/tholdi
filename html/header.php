<html lang="fr">
    <head>
        <meta charset ="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
        <title>THOLDI</title>
        <link rel="icon" type="image/x-icon" href="../image/minilogo.jpg">
        <link rel="stylesheet" href="../bootstrap-3.3.4-dist/css/bootstrap.min.css">
        <link rel="stylesheet" href="../css/styleBaseT.css">
    </head>
    <body>

        <header>
            <nav class="navbar navbar-inverse navbar-static-top" role="navigation">
                <div class="container">
                    <div class="row">
                        <div class="navbar-header">
                            <div class="col-lg-3 col-xs-3 col-md-3 col-sm-3">
                                <a href="../html/acceuilT.php" class="navbar-brand">
                                    <img src="../image/minilogo.jpg" id="logo">
                                </a>
                            </div>
                            <div class="col-xs-5 col-lg-5 col-sm-5  col-md-5 ">
                                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navegacion-fm">
                                    <span class="sr-only">Desplegar / Ocultar Menu</span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                </button>
                                <!-- Inicia Menu http://www.falconmasters.com/cursos/curso-sitio-web-bootstrap/ 11:28/2-->
                                <div class="collapse navbar-collapse" id="navegacion-fm">
                                    <ul class="nav navbar-nav">
                                        <li class="active"><a href="../html/acceuilTen.php">Home</a></li>
                                        <li><a href="../html/galleryTen.php">Gallery</a></li>
                                        <li><a href="../html/coResTen.php"> Co&amp;Booking</a></li>
                                        <li><a href="../html/acceuilT.php">Fr</a></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="col-xs-4  col-lg-4 col-sm-4 col-md-4">
                                <form action="" class="navbar-form navbar-right" role="search">
                                    <div class="form-group">
                                        <input type="text" class="form-control" placeholder="Recherche">
                                    </div>
                                    <button type="submit" class="btn btn-primary">
                                        <span class="glyphicon glyphicon-search"></span>
                                    </button>
                                </form>
                            </div>
                        </div>
                    </div>
            </nav>
        </header>

        <section class="jumbotron">
            <div class="container">
                <h1>To each his merchandise</h1>
                <p>Tholdi provides transportation for your goods!</p>
            </div>
        </section>