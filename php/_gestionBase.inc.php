<?php

session_start();
if (isset($_REQUEST["logout"])) {
    session_unset();
}
if (isset($_REQUEST["adrMel"]) && isset($_REQUEST["mdp"])) {
    $resultat = connexion($_REQUEST["adrMel"], $_REQUEST["mdp"]);
    if ($resultat == true) {
        $_SESSION["adrMel"] = $resultat["adrMel"];
        $_SESSION["mdp"] = $resultat["mdp"];
        $_SESSION["codeUser"] = $resultat["code"];
        $_SESSION["raisonSociale"] = $resultat["raisonSociale"];
    }
}


if ($_SERVER['PHP_SELF'] == "/projects/tholdi/html/coResT.php") {
    if (!(isset($_SESSION["adrMel"]) && isset($_SESSION["mdp"]))) {
        header("Location: ../html/inscriptionT.php");
    }
}

/**
 * Connexion à la base de données
 *
 * @return \PDO
 */
function gestionnaireDeConnexion() {
    $pdo = null;
    try {
        $pdo = new PDO(
                'mysql:host=localhost;dbname=tholdi2', 'root', '', array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8")
        );
    } catch (PDOException $err) {
        $messageErreur = $err->getMessage();
        error_log($messageErreur, 0);
    }
    return $pdo;
}

/**
 * Fonction pour reccuperer les pays
 *
 * @return type Array
 */
function obtenirPays() {
    $detailPays = false;
    $pdo = gestionnaireDeConnexion();
    if ($pdo != false) {
        $req = "Select * from Pays ";
        $resultat = $pdo->query($req);
        $detailPays = $resultat->fetchAll();
    }
    return $detailPays;
}

/**
 * Fontion pour récupérer les réservations passées par le client courant
 *
 * @return type Array
 */
function obtenirReservation() {
    $detailReservation = false;
    $pdo = gestionnaireDeConnexion();
    if ($pdo != false) {
        $req = "Select R.dateDebutReservation, R.dateFinReservation, R.volumeEstime "
                . "from reservation R "
                . "where R.codeReservation = (select max(codeReservation) from reservation)"
                . " And code = " . $_SESSION['codeUser'];
        $resultat = $pdo->query($req);
        $detailReservation = $resultat->fetchAll();
    }
    return $detailReservation;
}

/**
 * Fonction permttant de récupérer la ville de chargement de la réservation du client courant
 */
function obtenirVilleMiseDispoReservation() {
    $villeMiseDispo = false;
    $pdo = gestionnaireDeConnexion();
    if ($pdo != false) {
        $req = "Select * "
                . "from ville "
                . "where codeVille = (Select codeVilleMiseDispo from reservation where codeReservation = (select MAX(codeReservation) from Reservation)"
                . " And code = " . $_SESSION['codeUser'] . ")";
    }
}

/**
 * Fonction récupérant les ports de mise à disposition de la reservation courante passé par le client courant
 *
 * @return type Array
 */
function obtenirVilleMiseDispo() {
    $detailVilleMiseDispo = false;
    $pdo = gestionnaireDeConnexion();
    if ($pdo != false) {
        $req = "Select R.codeVilleMiseDispo, V.nomVille "
                . "From reservation R, ville V "
                . "Where R.codeReservation = (Select max(codeReservation) "
                . "From reservation) "
                . "And R.codeVilleMiseDispo = V.codeVille "
                . "And code =" . $_SESSION['codeUser'];
        $resultat = $pdo->query($req);
        $detailVilleMiseDispo = $resultat->fetchAll();
    }
    return $detailVilleMiseDispo;
}

/**
 * Fonction pour récupérer les ports d'arrvée de la reservation courante
 *
 * @return type Array
 */
function obtenirVilleRendre() {
    $detailVilleRndre = false;
    $pdo = gestionnaireDeConnexion();
    if ($pdo != false) {
        $req = "Select R.codeVilleRendre, V.nomVille "
                . "From reservation R, ville V "
                . "Where R.codeReservation = (select max(codeReservation) "
                . "From reservation) "
                . "And R.codeVilleRendre = V.codeVille "
                . "And code = " . $_SESSION['codeUser'];
        $resultat = $pdo->query($req);
        $detailVilleRndre = $resultat->fetchAll();
    }
    return $detailVilleRndre;
}

/**
 * Fonction de connexion
 *
 * @param type $adrMel
 * @param type $mdp
 * @return boolean
 */
function connexion($adrMel, $mdp) {
    $pdo = gestionnaireDeConnexion();
    if ($pdo != false) {
        $sql = "Select * "
                . "From personne "
                . "Where adrMel =:adrMel "
                . "And mdp =:mdp";
        $prep = $pdo->prepare($sql);
        $prep->bindParam(':adrMel', $adrMel, PDO::PARAM_STR);
        $prep->bindParam(':mdp', $mdp, PDO::PARAM_STR);
        $prep->execute();
        $resultat = $prep->fetch();
        if (is_array($resultat) && count($resultat) >= 1) {
            return $resultat;
        }
    }
    return false;
}

/**
 * Fonction pour obtenir l'ensemble des villes
 *
 * @return type Array
 */
function obtenirVille() {
    $detailVille = false;
    $pdo = gestionnaireDeConnexion();
    if ($pdo != false) {
        $req = "Select * "
                . "From Ville ";
        $resultat = $pdo->query($req);
        $detailVille = $resultat->fetchAll();
    }
    return $detailVille;
}

/**
 * Fonction pour otenir l'ensemble des conteneurs
 *
 * @return type Array
 */
function obtenirConteneur() {
    $detailTypeConteneur = false;
    $pdo = gestionnaireDeConnexion();
    if ($pdo != false) {
        $req = "Select * "
                . "From Typecontainer ";
        $resultat = $pdo->query($req);
        $detailTypeConteneur = $resultat->fetchAll();
    }
    return $detailTypeConteneur;
}

/**
 * Fonction pour obtenir les types de conteneur
 *
 * @return type Array
 */
function obtenirAttribut() {
    $detailAttribut = false;
    $pdo = gestionnaireDeConnexion();
    if ($pdo != false) {
        $req = "Select * "
                . "From Typecontainer ";
        $resultat = $pdo->query($req);
        $detailAttribut = $resultat->fetchAll();
    }
    return $detailAttribut;
}

/**
 * Fonction pour obtenir les utilisateurs
 *
 * @return type Array
 */
function obtenirPersonne() {
    $detailPersonne = false;
    $pdo = gestionnaireDeConnexion();
    if ($pdo != false) {
        $req = "Select * "
                . "From personne ";
        $resultat = $pdo->query($req);
        $detailPersonne = $resultat->fetchAll();
    }
    return $detailPersonne;
}

/**
 * Fonction d'inscription
 *
 * @param type $code
 * @param type $raisonSociale
 * @param type $adresse
 * @param type $cp
 * @param type $ville
 * @param type $adrMel
 * @param type $telephone
 * @param type $contact
 * @param type $codePays
 * @param type $mdp
 * 
 * @return boolean
 */
function inscription($code, $raisonSociale, $adresse, $cp, $ville, $adrMel, $telephone, $contact, $codePays, $mdp) {
    $reussi = false;
    $pdo = gestionnaireDeConnexion();
    if ($pdo != false) {
        $code = $pdo->lastInsertId();
        $raisonSociale = $pdo->quote($raisonSociale);
        $adresse = $pdo->quote($adresse);
        $cp = $pdo->quote($cp);
        $ville = $pdo->quote($ville);
        $adrMel = $pdo->quote($adrMel);
        $telephone = $pdo->quote($telephone);
        $contact = $pdo->quote($contact);
        $codePays = $pdo->quote($codePays);
        $mdp = $pdo->quote($mdp);

        $req = "Insert into personne "
                . "Values($code, $raisonSociale, $adresse, $cp, $ville, $adrMel, $telephone, $contact, $codePays, $mdp)";
        $resultat = $pdo->exec($req);
        if ($resultat == 1) {
            $reussi = true;
        }
    }
    return $reussi;
}

/**
 * Fonction de création d'une réservation
 *
 * @param type $dateDebutReservation
 * @param type $dateFinReservation
 * @param type $volumeEstime
 * @param type $codeVilleMiseDispo
 * @param type $codeVilleRendre
 * @param type $code
 */
function reservation($dateDebutReservation, $dateFinReservation, $volumeEstime, $codeVilleMiseDispo, $codeVilleRendre, $code) {
    $succes = false;
    $pdo = gestionnaireDeConnexion();
    if ($pdo != false) {
        $dateDebutReservation = $pdo->quote($dateDebutReservation);
        $dateFinReservation = $pdo->quote($dateFinReservation);
        $volumeEstime = $pdo->quote($volumeEstime);
        $codeVilleMiseDispo = $pdo->quote($codeVilleMiseDispo);
        $codeVilleRendre = $pdo->quote($codeVilleRendre);
        $code = $pdo->quote($code);

        $req = "Insert into RESERVATION (dateDebutReservation, dateFinReservation, dateReservation, volumeEstime, codeVilleMiseDispo, codeVilleRendre, code) "
                . "Values ($dateDebutReservation, $dateFinReservation, curdate(), $volumeEstime, $codeVilleMiseDispo , $codeVilleRendre, $code)";
        $resultat = $pdo->exec($req);
        if ($resultat == 1) {
            $succes = true;
        }
        //retourner le code de la réservation crée Cf last_insertId() fcontion pdo
    }
}

/**
 * Fonction permettant de réserver
 *
 * @param type $codeReservation
 * @param type $typeContainer
 * @param type $qteReserver
 */
function reserver($codeReservation, $typeContainer, $qteReserver) {
    $reussi = false;
    $pdo = gestionnaireDeConnexion();
    if ($pdo != false) {
        $codeReservation = $pdo->quote($codeReservation);
        $typeContainer = $pdo->quote($typeContainer);
        $qteReserver = $pdo->quote($qteReserver);

        $req = "Insert into reserver "
                . "Values($codeReservation, $typeContainer, $qteReserver)";
        $resultat = $pdo->exec($req);
        if ($resultat == 1) {
            $reussi = true;
        }
    }
}

/**
 * Fonction permettant de récupérer la dernière reservation du client courant
 *
 * @return type int
 */
function recuperationCodeReservation() {
    $pdo = gestionnaireDeConnexion();
    if ($pdo != false) {
        $req = "select Max(codeReservation) as'codeReservation' "
                . "From reservation"
                . " Where code = " . $_SESSION['codeUser'];
        $resultat = $pdo->query($req);
    }
    return $resultat;
}

/**
 * Fonction permettant de récpurer le conteneur  correspondant à a dernière réservation éffectuée par le cient courant
 *
 * @return type boolean
 */
function afficherConteneur() {
    $resultat = false;
    $pdo = gestionnaireDeConnexion();
    if ($pdo != false) {
        $req = "Select T.*, R.qteReserver, (T.prix * R.qteReserver) as mtContainer "
                . "From typeContainer T, reserver R "
                . "Where R.codeReservation = (Select Max(codeReservation) "
                . "From reservation "
                . "Where code = " . $_SESSION['codeUser'] . ") "
                . "And R.typeContainer=T.typeContainer";
        $resultat = $pdo->query($req);
    }
    return $resultat;
}

/**
 * Fonction permettant de récupérer le client courant
 *
 * @return type boolean
 */
function afficherClient() {
    $resultat = false;
    $pdo = gestionnaireDeConnexion();
    if ($pdo != false) {
        $req = "Select * "
                . "From personne "
                . "Where code = " . $_SESSION["codeUser"];
        $resultat = $pdo->query($req);
    }
    return $resultat;
}

/**
 * Obtenir les containers reservés du client courant
 *
 * @return type Array
 */
function obtenirConteneurReserve() {
    $detailTypeConteneurReserve = false;
    $pdo = gestionnaireDeConnexion();
    if ($pdo != false) {
        $req = "Select * "
                . "From Typecontainer "
                . "Where typeContainer in (Select typeContainer "
                . "From Reserver "
                . "Where codeReservation = (Select Max(codeReservation) "
                . "From Reservation "
                . "Where code = ". $_SESSION['codeUser']."))";
        $resultat = $pdo->query($req);
        $detailTypeConteneurReserve = $resultat->fetchAll();
    }
    return $detailTypeConteneurReserve;
}

/**
 * Obtenir le nombre de container réservés par le client courant
 *
 * @return type Array
 */
function obtenirQuantite() {
    $detailQuantite = false;
    $pdo = gestionnaireDeConnexion();
    if ($pdo != false) {
        $req = "Select * "
                . "From Reserver "
                . "Where codeReservation = (Select Max(codeReservation) "
                . "From Reservation "
                . "Where code = ". $_SESSION['codeUser'] . ")";
        $resultat = $pdo->query($req);
        $detailQuantite = $resultat->fetchAll();
    }
    return $detailQuantite;
}

/**
 * Fonction pour obtenir le volume pour une reservation pour le client courant
 *
 * @return type Array
 */
function obtenirVolume() {
    $detailVolume = false;
    $pdo = gestionnaireDeConnexion();
    if ($pdo != false) {
        $req = "Select * "
                . "From Reservation "
                . "Where codeReservation = (Select Max(codeReservation) "
                . "From Reservation "
                . "Where code = ". $_SESSION['codeUser'].")";
        $resultat = $pdo->query($req);
        var_dump($resultat);
        $detailVolume = $resultat->fetchAll();
    }
    return $detailVolume;
}

/**
 * Fonction permettant de créer un devis
 *
 * @return boolean
 */
function devis() {
    $reussi = false;
    $pdo = gestionnaireDeConnexion();
    $quantite = 0;
    $prix = 0;
    if ($pdo != false) {
        $obtenirQuantite = obtenirQuantite();
        //CALCUL DE LA QUANTITE TOTAL DES CONTAINER
        for ($i = 0; $i < count($obtenirQuantite); $i++) {
            $uneQuantite = $obtenirQuantite[$i]["qteReserver"];
            $quantite = $uneQuantite + $uneQuantite;
        }
        $obtenirPrix = obtenirConteneurReserve();
        //CALCUL DU PRIX TOTAL DES CONTAINERS
        for ($j = 0; $j < count($obtenirPrix); $j++) {
            $unPrix = $obtenirPrix[$j]["prix"];
            $prix = $unPrix + $unPrix;
        }
        //CALCUL DU MONTANT TOAL DU DEVIS
        $montantDevis = $quantite * $prix;
        $obtennirVolume = obtenirVolume();
        $volume = $obtennirVolume[0]["volumeEstime"];
        $nbContainer = $quantite;
        $reqInsert = "Insert into DEVIS (montantDevis, volume, nbContainers) "
                . "Values ($montantDevis, $volume, $nbContainer)";
        $resultatInsert = $pdo->exec($reqInsert);
        if ($resultatInsert == 1) {
            $reussi = true;
        }
        $reqUpdate = "Update reservation "
                . "Set codeDevis = (Select MAX(codeDevis) "
                . "From DEVIS) "
                . "Where code = " . $_SESSION["codeUser"] . " "
                . "And codeReservation = (Select MAX(codeReservation) "
                . "                                                 From reserver)";
        $resultatUpdate = $pdo->exec($reqUpdate);
        if ($resultatUpdate == 1) {
            $reussi = true;
        }
    }
    return $reussi;
}

/**
 * Fonction permettant d'afficher le montant total sur le devis
 *
 * @return type Array
 */
function afficherMontant() {
    $detailDevis = false;
    $pdo = gestionnaireDeConnexion();
    if ($pdo != false) {
        $req = "Select montantDevis "
                . "From Devis "
                . "Where codeDevis = (Select codeDevis "
                . "From Reservation "
                . "Where code = " . $_SESSION["codeUser"] . " "
                . "And codeReservation = (Select Max(codeReservation) "
                . "from Reservation))";
        $resultat = $pdo->query($req);
        var_dump($resultat);
        $detailDevis = $resultat->fetchAll();
    }
    return $detailDevis;
}

/**
 * Fonction permettant de supprimer un ou plusieurs type de container
 *
 * @return boolean
 */
function supprimerContainer() {
    $reussi = false;
    $pdo = gestionnaireDeConnexion();

    if ($pdo != false) {
        foreach ($_POST['typeContainer'] as $value) {
            $req = "DELETE FROM reserver "
                    . "WHERE typeContainer = " . "'" . $value . "'"
                    . "AND codeReservation = (SELECT MAX(codeReservation)"
                    . " FROM reservation"
                    . " WHERE code = " . $_SESSION['codeUser'] . ")";
            $resultat = $pdo->exec($req);
            if ($resultat != 0) {
                $reussi = true;
            }
        }
        $reussi = true;
    }
    return $reussi;
}

/**
 * Fonction permettant de modifier la réservation courante
 *
 * @param type $dateDebutReservation
 * @param type $dateFinReservation
 * @param type $volumeEstime
 * @param type $codeVilleMiseDispo
 * @param type $codeVilleRendre
 *
 * @return boolean
 */
function modifierReservation($dateDebutReservation, $dateFinReservation, $volumeEstime, $codeVilleMiseDispo, $codeVilleRendre) {
    $succes = false;
    $pdo = gestionnaireDeConnexion();
    if ($pdo != false) {
        $dateDebutReservation = $pdo->quote($dateDebutReservation);
        $dateFinReservation = $pdo->quote($dateFinReservation);
        $volumeEstime = $pdo->quote($volumeEstime);
        $codeVilleMiseDispo = $pdo->quote($codeVilleMiseDispo);
        $codeVilleRendre = $pdo->quote($codeVilleRendre);

        $req = "UPDATE reservation SET dateDebutReservation = " . $dateDebutReservation . ", dateFinReservation = " . $dateFinReservation . ", volumeEstime = " . $volumeEstime . ", codeVilleMiseDispo = " . $codeVilleMiseDispo . ", codeVilleRendre = " . $codeVilleRendre . " "
                . "where codeReservation = (Select MAX(codeReservation) from reserver) "
                . "And code = ". $_SESSION['codeUser'];
        $resultatModificationReservation = $pdo->exec($req);
        if ($resultatModificationReservation == 1) {
            $succes = true;
        }
        return $succes;
    }
}

/**
 * Fonction permettant de modifier la quantité d'un ou plusieurs containers
 *
 * @param type $qteReserver
 * 
 * @return boolean
 */
function modifierQteContainer($qteReserver) {
    $succes = false;
    $pdo = gestionnaireDeConnexion();
    if ($pdo != false) {
        $type = typeContainerResrve();

        foreach ($_POST['qteReserver'] as $value) {
            $req = "UPDATE reserver "
                    . "SET qteReserver = " . $value . " "
                    . "WHERE codeReservation = (Select MAX(codeReservation) From reservation Where code = " . $_SESSION["codeUser"] . ") "
                    . "AND typeContainer = " . $type[0];
            var_dump($req);
//            $resultat = $pdo->exec($req);
//            if ($resultat != 0) {
//                $succes = true;
//            }
        }
    }
    return $succes;
}
