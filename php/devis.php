<?php
require('../fpdf/fpdf.php');
define('EURO', chr(128));
define('EURO_VAL', 6.55957);

// Xavier Nicolay 2004
// Version 1.02
//
// Reste à faire :
// + Multipage (gestion automatique sur plusieurs pages)
// + Ajout de logo
// 
//////////////////////////////////////
// fonctions à utiliser (publiques) //
//////////////////////////////////////
//  function tailleTexte( $texte, $larg )
//  function ajouterSociete( $nom, $adresse )
//  function fact_dev( $libelle, $num )
//  function ajouterDevis( $numdev )
//  function ajouterFacture( $numfact )
//  function ajouterDate( $date )
//  function ajouterClient( $ref )
//  function ajouterPage( $page )
//  function ajouterAdresseClient( $adresse )
//  function ajouterReglement( $mode )
//  function ajouterEcheance( $date )
//  function ajouterTVA($tva)
//  function ajoulterReference($ref)
//  function ajouterColones( $tab )
//  function ajouterFromat( $tab )
//  function lineVert( $tab )
//  function ajouterLigne( $ligne, $tab )
//  function ajouterRemarque($remarque)
//  function ajouterCadreTVA()
//  function ajouterCadreEuros()
//  function ajouterTVAs( $params, $tab_tva, $invoice )
//  function temporaire( $texte )

class PDF extends FPDF {

// En-tête
    function Header() {
        // Logo
        $this->Image('logo.png', 10, 6, 30);
        // Police Arial gras 15
        $this->SetFont('Arial', 'B', 15);
        // Décalage à droite
        $this->Cell(80);
        // Titre
        $this->Cell(30, 10, 'Titre', 1, 0, 'C');
        // Saut de ligne
        $this->Ln(20);
    }

// Pied de page
    function Footer() {
        // Positionnement à 1,5 cm du bas
        $this->SetY(-15);
        // Police Arial italique 8
        $this->SetFont('Arial', 'I', 8);
        // Numéro de page
        $this->Cell(0, 10, 'Page ' . $this->PageNo() . '/{nb}', 0, 0, 'C');
    }

}

class PDF_Invoice extends FPDF {

// variables privées
    var $colonnes;
    var $format;
    var $angle = 0;

// fonctions privées
    function RoundedRect($x, $y, $w, $h, $r, $style = '') {
        $k = $this->k;
        $hp = $this->h;
        if ($style == 'F')
            $op = 'f';
        elseif ($style == 'FD' || $style == 'DF')
            $op = 'B';
        else
            $op = 'S';
        $MyArc = 4 / 3 * (sqrt(2) - 1);
        $this->_out(sprintf('%.2F %.2F m', ($x + $r) * $k, ($hp - $y) * $k));
        $xc = $x + $w - $r;
        $yc = $y + $r;
        $this->_out(sprintf('%.2F %.2F l', $xc * $k, ($hp - $y) * $k));

        $this->_Arc($xc + $r * $MyArc, $yc - $r, $xc + $r, $yc - $r * $MyArc, $xc + $r, $yc);
        $xc = $x + $w - $r;
        $yc = $y + $h - $r;
        $this->_out(sprintf('%.2F %.2F l', ($x + $w) * $k, ($hp - $yc) * $k));
        $this->_Arc($xc + $r, $yc + $r * $MyArc, $xc + $r * $MyArc, $yc + $r, $xc, $yc + $r);
        $xc = $x + $r;
        $yc = $y + $h - $r;
        $this->_out(sprintf('%.2F %.2F l', $xc * $k, ($hp - ($y + $h)) * $k));
        $this->_Arc($xc - $r * $MyArc, $yc + $r, $xc - $r, $yc + $r * $MyArc, $xc - $r, $yc);
        $xc = $x + $r;
        $yc = $y + $r;
        $this->_out(sprintf('%.2F %.2F l', ($x) * $k, ($hp - $yc) * $k));
        $this->_Arc($xc - $r, $yc - $r * $MyArc, $xc - $r * $MyArc, $yc - $r, $xc, $yc - $r);
        $this->_out($op);
    }

    function _Arc($x1, $y1, $x2, $y2, $x3, $y3) {
        $h = $this->h;
        $this->_out(sprintf('%.2F %.2F %.2F %.2F %.2F %.2F c ', $x1 * $this->k, ($h - $y1) * $this->k, $x2 * $this->k, ($h - $y2) * $this->k, $x3 * $this->k, ($h - $y3) * $this->k));
    }

    function Rotate($angle, $x = -1, $y = -1) {
        if ($x == -1)
            $x = $this->x;
        if ($y == -1)
            $y = $this->y;
        if ($this->angle != 0)
            $this->_out('Q');
        $this->angle = $angle;
        if ($angle != 0) {
            $angle *= M_PI / 180;
            $c = cos($angle);
            $s = sin($angle);
            $cx = $x * $this->k;
            $cy = ($this->h - $y) * $this->k;
            $this->_out(sprintf('q %.5F %.5F %.5F %.5F %.2F %.2F cm 1 0 0 1 %.2F %.2F cm', $c, $s, -$s, $c, $cx, $cy, -$cx, -$cy));
        }
    }

    function _endpage() {
        if ($this->angle != 0) {
            $this->angle = 0;
            $this->_out('Q');
        }
        parent::_endpage();
    }

    /**
     * Fonction permettant de gérer la taille du texte
     * 
     * @param type $texte
     * @param type $largeur
     * @return type
     */
    function tailleTexte($texte, $largeur) {
        $index = 0;
        $nb_lines = 0;
        $loop = TRUE;
        while ($loop) {
            $pos = strpos($texte, "\n");
            if (!$pos) {
                $loop = FALSE;
                $ligne = $texte;
            } else {
                $ligne = substr($texte, $index, $pos);
                $texte = substr($texte, $pos + 1);
            }
            $length = floor($this->GetStringWidth($ligne));
            $res = 1 + floor($length / $largeur);
            $nb_lines += $res;
        }
        return $nb_lines;
    }

    /**
     * Fonction permettant d'affiche le nom de la société ainsi que ces coordonnées en haut à gauche
     * 
     * @param type $nom
     * @param type $adresse
     */
    function ajouterSociete($nom, $adresse) {
        $x1 = 10;
        $y1 = 12;
        //Positionnement en bas
        $this->SetXY($x1, $y1);
        $this->SetFont('Arial', 'B', 12);
        $this->SetXY($x1, $y1 + 4);
        $this->SetFont('Arial', '', 10);
        $length = $this->GetStringWidth($adresse);
        //Coordonnées de la société
        $lignes = $this->tailleTexte($adresse, $length);
        $this->MultiCell($length, 4, $adresse);
    }

    /**
     * Fonction permetant d'afficher le libelle et le numéro du devis
     * 
     * @param type $libelle
     * @param type $num
     */
    function fact_dev($libelle, $num) {
        $r1 = $this->w - 70;
        $r2 = $r1 + 68;
        $y1 = 6;
        $y2 = $y1 + 2;
        $mid = ($r1 + $r2 ) / 2;

        $texte = $libelle . $num;
        $szfont = 12;
        $loop = 0;

        while ($loop == 0) {
            $this->SetFont("Arial", "B", $szfont);
            $sz = $this->GetStringWidth($texte);
            if (($r1 + $sz) > $r2)
                $szfont --;
            else
                $loop ++;
        }

        $this->SetLineWidth(0.1);
        $this->SetFillColor(192);
        $this->RoundedRect($r1, $y1, ($r2 - $r1), $y2, 2.5, 'DF');
        $this->SetXY($r1 + 1, $y1 + 2);
        $this->Cell($r2 - $r1 - 1, 5, $texte, 0, 0, "C");
    }

    /**
     * Fonction permettant de générer automatiquement un numéro de devis
     * 
     * @param type $numdev
     */
    function ajouterDevis($numdev) {
        $string = sprintf("Devis ");
        $this->fact_dev($string, $numdev);
    }

    /**
     * Fonction permettant de générer un numéro de facture
     * 
     * @param type $numfact
     */
    function ajouterFacture($numfact) {
        $string = sprintf("FA%04d", $numfact);
        $this->fact_dev("Facture", $string);
    }

    /**
     * Fonction permettant d'afficher un cadre avec la date du devis
     * 
     * @param type $date
     */
    function ajouterDate($date) {
        $r1 = $this->w - 61;
        $r2 = $r1 + 30;
        $y1 = 50;
        $y2 = $y1;
        $this->SetXY($r1 + ($r2 - $r1) / 2 - 5, $y1 + 9);
        $this->SetFont("Arial", "", 10);
        $this->Cell(10, 5, $date, 0, 0, "C");
    }

    /**
     * Fonction permettant d'afficher un cadre avec les reference du client
     * 
     * @param type $raisonSociale
     */
    function ajouterClient($raisonSociale) {
        $r1 = $this->w - 37;
        $r2 = $r1;
        $y1 = 30;
        $y2 = $y1;
        $mid = $y1 + ($y2 / 2);
        $this->SetXY($r1 + ($r2 - $r1) / 2 - 5, $y1 + 9);
        $this->SetFont("Arial", "", 10);
        $this->Cell(10, 5, $raisonSociale, 0, 0, "C");
    }

    /**
     * Fonction permettant d'afficher l'adresse du client
     * 
     * @param type $adresse
     */
    function ajouterAdresseClient($adresse) {
        $r1 = $this->w - 58;
        $r2 = $r1;
        $y1 = 45;
        $this->SetXY($r1, $y1);
        $this->MultiCell(60, 4, $adresse);
    }

    /**
     * Fonction permettant d'afficher le code postal du client
     * 
     * @param type $cp
     */
    function ajouterCodePostalClient($cp) {
        $r1 = $this->w - 65;
        $r2 = $r1;
        $y1 = 50;
        $this->SetXY($r1, $y1);
        $this->MultiCell(60, 4, $cp);
    }

    /**
     * Fonction permettant d'afficher la ville du client
     * @param type $ville
     */
    function ajouterVilleClient($ville) {
        $r1 = $this->w - 53;
        $r2 = $r1;
        $y1 = 50;
        $this->SetXY($r1, $y1);
        $this->MultiCell(60, 4, $ville);
    }

    /**
     * Fonction permettant d'afficher un cadre avec la date d'échéance
     * 
     * @param type $date
     */
    function ajouterEcheance($date) {
        $r1 = 80;
        $r2 = $r1 + 40;
        $y1 = 80;
        $y2 = $y1 + 10;
        $mid = $y1 + (($y2 - $y1) / 2);
        $this->RoundedRect($r1, $y1, ($r2 - $r1), ($y2 - $y1), 2.5, 'D');
        $this->Line($r1, $mid, $r2, $mid);
        $this->SetXY($r1 + ($r2 - $r1) / 2 - 5, $y1 + 1);
        $this->SetFont("Arial", "B", 10);
        $this->Cell(10, 4, "DATE D'ECHEANCE", 0, 0, "C");
        $this->SetXY($r1 + ($r2 - $r1) / 2 - 5, $y1 + 5);
        $this->SetFont("Arial", "", 10);
        $this->Cell(10, 5, $date, 0, 0, "C");
    }

    /**
     * Affiche un cadre avec le numéro de TVA
     * 
     * @param type $tva
     */
    function ajouterTVA($tva) {
        $this->SetFont("Arial", "B", 10);
        $r1 = $this->w - 80;
        $r2 = $r1 + 70;
        $y1 = 80;
        $y2 = $y1 + 10;
        $mid = $y1 + (($y2 - $y1) / 2);
        $this->RoundedRect($r1, $y1, ($r2 - $r1), ($y2 - $y1), 2.5, 'D');
        $this->Line($r1, $mid, $r2, $mid);
        $this->SetXY($r1 + 16, $y1 + 1);
        $this->Cell(40, 4, "TVA Intracommunautaire", '', '', "C");
        $this->SetFont("Arial", "", 10);
        $this->SetXY($r1 + 16, $y1 + 5);
        $this->Cell(40, 5, $tva, '', '', "C");
    }

    /**
     * Fonction permettant d'afficher une ligne avec des references en haut à gauche
     * 
     * @param type $ref
     */
    function ajouterReferences($ref) {
        $this->SetFont("Arial", "", 10);
        $length = $this->GetStringWidth("References : " . $ref);
        $r1 = 10;
        $r2 = $r1 + $length;
        $y1 = 92;
        $y2 = $y1 + 5;
        $this->SetXY($r1, $y1);
        $this->Cell($length, 4, "References : " . $ref);
    }

    /**
     * Fonction permettant de traver les colonnes du devis/facture
     * 
     * @global type $colonnes
     * @param type $tab
     */
    function ajouterColones($tab) {
        global $colonnes;

        $r1 = 10;
        $r2 = $this->w - ($r1 * 2);
        $y1 = 100;
        $y2 = $this->h - 50 - $y1;
        $this->SetXY($r1, $y1);
        $this->Rect($r1, $y1, $r2, $y2, "D");
        $this->Line($r1, $y1 + 6, $r1 + $r2, $y1 + 6);
        $colX = $r1;
        $colonnes = $tab;
        while (list( $lib, $pos ) = each($tab)) {
            $this->SetXY($colX, $y1 + 2);
            $this->Cell($pos, 1, $lib, 0, 0, "C");
            $colX += $pos;
            $this->Line($colX, $y1, $colX, $y1 + $y2);
        }
    }

// mémorise le format (gauche, centre, droite) d'une colonne
    function ajouterFromat($tab) {
        global $format, $colonnes;

        while (list( $lib, $pos ) = each($colonnes)) {
            if (isset($tab["$lib"]))
                $format[$lib] = $tab["$lib"];
        }
    }

    function lineVert($tab) {
        global $colonnes;

        reset($colonnes);
        $maxSize = 0;
        while (list( $lib, $pos ) = each($colonnes)) {
            $texte = $tab[$lib];
            $longCell = $pos - 2;
            $size = $this->tailleTexte($texte, $longCell);
            if ($size > $maxSize)
                $maxSize = $size;
        }
        return $maxSize;
    }

    /**
     * Fonction permettant d'ajouter ou uneligne au devis
     * 
     * @global type $colonnes
     * @global type $format
     * @param type $ligne
     * @param type $tab
     * @return type
     */
    function ajouterLigne($ligne, $tab) {
        global $colonnes, $format;

        $ordonnee = 10;
        $maxSize = $ligne;

        reset($colonnes);
        while (list( $lib, $pos ) = each($colonnes)) {
            $longCell = $pos - 2;
            $texte = $tab[$lib];
            $length = $this->GetStringWidth($texte);
            $tailleTexte = $this->tailleTexte($texte, $length);
            $formText = $format[$lib];
            $this->SetXY($ordonnee, $ligne - 1);
            $this->MultiCell($longCell, 4, $texte, 0, $formText);
            if ($maxSize < ($this->GetY() ))
                $maxSize = $this->GetY();
            $ordonnee += $pos;
        }
        return ( $maxSize - $ligne );
    }

    /**
     * Fonction permettant d'ajouter un commentaire en bas à gauche
     * 
     * @param type $remarque
     */
    function ajouterRemarque($remarque) {
        $this->SetFont("Arial", "", 10);
        $length = $this->GetStringWidth("Remarque : " . $remarque);
        $r1 = 10;
        $r2 = $r1 + $length;
        $y1 = $this->h - 45.5;
        $y2 = $y1 + 5;
        $this->SetXY($r1, $y1);
        $this->Cell($length, 4, "Remarque : " . $remarque);
    }

    /**
     * Fonction permettant de créer le carde de taux de TVA
     */
    function ajouterCadreTVA() {
        $this->SetFont("Arial", "B", 8);
        $r1 = 10;
        $r2 = $r1 + 120;
        $y1 = $this->h - 40;
        $y2 = $y1 + 20;
        $this->RoundedRect($r1, $y1, ($r2 - $r1), ($y2 - $y1), 2.5, 'D');
        $this->Line($r1, $y1 + 4, $r2, $y1 + 4);
        $this->Line($r1 + 5, $y1 + 4, $r1 + 5, $y2); // avant BASES HT
        $this->Line($r1 + 27, $y1, $r1 + 27, $y2);  // avant REMISE
        $this->Line($r1 + 43, $y1, $r1 + 43, $y2);  // avant MT TVA
        $this->Line($r1 + 63, $y1, $r1 + 63, $y2);  // avant % TVA
        $this->Line($r1 + 75, $y1, $r1 + 75, $y2);  // avant PORT
        $this->Line($r1 + 91, $y1, $r1 + 91, $y2);  // avant TOTAUX
        $this->SetXY($r1 + 9, $y1);
        $this->Cell(10, 4, "BASES HT");
        $this->SetX($r1 + 29);
        $this->Cell(10, 4, "REMISE");
        $this->SetX($r1 + 48);
        $this->Cell(10, 4, "MT TVA");
        $this->SetX($r1 + 63);
        $this->Cell(10, 4, "% TVA");
        $this->SetX($r1 + 78);
        $this->Cell(10, 4, "PORT");
        $this->SetX($r1 + 100);
        $this->Cell(10, 4, "TOTAUX");
        $this->SetFont("Arial", "B", 6);
        $this->SetXY($r1 + 93, $y2 - 8);
        $this->Cell(6, 0, "H.T.   :");
        $this->SetXY($r1 + 93, $y2 - 3);
        $this->Cell(6, 0, "T.V.A. :");
    }

    /**
     * Fonction permettant de créer le carder des totaux
     */
    function ajouterCadreEuros() {
        $r1 = $this->w - 70;
        $r2 = $r1 + 60;
        $y1 = $this->h - 40;
        $y2 = $y1 + 20;
        $this->RoundedRect($r1, $y1, ($r2 - $r1), ($y2 - $y1), 2.5, 'D');
        $this->Line($r1 + 20, $y1, $r1 + 20, $y2); // avant EUROS
        $this->Line($r1 + 20, $y1 + 4, $r2, $y1 + 4); // Sous Euros
        $this->SetFont("Arial", "B", 8);
        $this->SetXY($r1 + 22, $y1);
        $this->Cell(20, 4, "EUROS", 0, 0, "C");
        $this->SetFont("Arial", "", 8);
        $this->SetXY($r1, $y1 + 5);
        $this->Cell(20, 10, "TOTAL TTC", 0, 0, "C");
        $this->Cell(30, 10, "" , 0, 0, "C");
    }

    /**
     * Permet de rajouter un commentaire en sous-impression
     * ATTENTION: APPALER CETTE FONCTION EN PREMIER
     * 
     * @param type $texte
     */
    function temporaire($texte) {
        $this->SetFont('Arial', 'B', 50);
        $this->SetTextColor(203, 203, 203);
        $this->Rotate(45, 55, 190);
        $this->Text(55, 190, $texte);
        $this->Rotate(0);
        $this->SetTextColor(0, 0, 0);
    }
}
?>