<?php
include("_gestionBase.inc.php");

if (isset($_REQUEST)) {
    $dateDebutReservation = $_REQUEST['dateDebutReservation'];
    $dateFinReservation = $_REQUEST['dateFinReservation'];
    $volumeEstime = $_REQUEST['volumeEstime'];
    $codeVilleMiseDispo = $_REQUEST['codeVilleMiseDispo'];
    $codeVilleRendre = $_REQUEST['codeVilleRendre'];
}

$reussi = modifierReservation($dateDebutReservation, $dateFinReservation, $volumeEstime, $codeVilleMiseDispo, $codeVilleRendre);

if(isset($_REQUEST)) {
    $qteReserver = $_REQUEST['qteReserver'];
    
    $resultatModificationQteContainer = modifierQteContainer($qteReserver);
    if($resultatModificationQteContainer == 1) {
        header("Location:../html/coResT2.php");
    }
}
?>

