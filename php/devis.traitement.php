<?php
require ('../php/_gestionBase.inc.php');
require ('../php/devis.php');
$date=  date('d/m/Y');
$pdf = new PDF_Invoice('P', 'mm', 'A4');
$pdf->AddPage();
$pdf->Image('minilogo.jpg',10,6,30);
$pdf->ajouterSociete("THOLDI", "5 Rue du Pressoir\n78570 CHANTELOUP\nFRANCE");
$pdf->ajouterDevis(01);

$collectionClient = afficherClient();
if ($collectionClient != null) {
    foreach ($collectionClient as $clientCourant) {
        $raisonSociale = $clientCourant["raisonSociale"];
        $pdf->ajouterClient($raisonSociale);

        $adresse = $clientCourant["adresse"];
        $pdf->ajouterAdresseClient($adresse);
        $cp = $clientCourant["cp"];
        $pdf->ajouterCodePostalClient($cp);
        $ville=$clientCourant["ville"];
        $pdf->ajouterVilleClient($ville);
    }
}

$pdf->ajouterReferences("devis 1 du: ".$date);
$cols = array("REFERENCE" => 23,
    "DESIGNATION" => 78,
    "QUANTITE" => 22,
    "P.U. HT" => 26,
    "MONTANT H.T." => 30,
    "TVA" => 11);
$pdf->ajouterColones($cols);

$cols = array("REFERENCE" => "L",
    "DESIGNATION" => "L",
    "QUANTITE" => "C",
    "P.U. HT" => "R",
    "MONTANT H.T." => "R",
    "TVA" => "C");
$pdf->ajouterFromat($cols);
$pdf->ajouterFromat($cols);

$y = 109;
$collectionTypeContainer = afficherConteneur();
if ($collectionTypeContainer != null) {
    foreach ($collectionTypeContainer as $conteneurCourant) {
        $line = array(
            "REFERENCE" => $conteneurCourant["typeContainer"],
            "DESIGNATION" => $conteneurCourant["libelleTypeContainer"],
            "QUANTITE" => $conteneurCourant["qteReserver"],
            "P.U. HT" => $conteneurCourant["prix"],
            "MONTANT H.T." => $conteneurCourant["mtContainer"],
            "TVA" => 20
        );
        $taille = $pdf->ajouterLigne($y, $line);
        $y += $taille + 2;
    }
}

$tot_prods = array(
    array("px_unit" => 600, "qte" => 1, "tva" => 1),
    array("px_unit" => 10, "qte" => 1, "tva" => 1)
);


$pdf->ajouterCadreEuros();
$pdf->Output();
