-- phpMyAdmin SQL Dump
-- version 4.1.4
-- http://www.phpmyadmin.net
--
-- Client :  127.0.0.1
-- Généré le :  Mer 29 Mars 2017 à 23:40
-- Version du serveur :  5.6.15-log
-- Version de PHP :  5.4.24

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données :  `tholdi2`
--

-- --------------------------------------------------------

--
-- Structure de la table `devis`
--

CREATE TABLE IF NOT EXISTS `devis` (
  `codeDevis` smallint(5) NOT NULL AUTO_INCREMENT,
  `dateDevis` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `montantDevis` decimal(10,2) NOT NULL,
  `volume` decimal(10,0) DEFAULT NULL,
  `nbContainers` decimal(10,0) DEFAULT NULL,
  `valider` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`codeDevis`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Structure de la table `pays`
--

CREATE TABLE IF NOT EXISTS `pays` (
  `codePays` char(4) COLLATE utf8_bin NOT NULL DEFAULT '',
  `nomPays` varchar(30) COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`codePays`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Contenu de la table `pays`
--

INSERT INTO `pays` (`codePays`, `nomPays`) VALUES
('ALL', 'Allemagne'),
('ARG', 'Argentine'),
('AUS', 'Australie'),
('BEL', 'Belgique'),
('BRE', 'Brésil'),
('CHI', 'Chine'),
('ESP', 'Espagne'),
('FRA', 'France'),
('GB', 'Grande-Bretagne'),
('ITA', 'Italie'),
('JAP', 'Japon'),
('KUW', 'Kuwait'),
('MAD', 'Madagascar'),
('MEX', 'Mexique'),
('PB', 'Pays-Bas'),
('SUE', 'Suede'),
('USA', 'Etats-Unis');

-- --------------------------------------------------------

--
-- Structure de la table `personne`
--

CREATE TABLE IF NOT EXISTS `personne` (
  `code` smallint(8) NOT NULL AUTO_INCREMENT,
  `raisonSociale` varchar(50) COLLATE utf8_bin NOT NULL,
  `adresse` varchar(80) COLLATE utf8_bin NOT NULL,
  `cp` char(5) COLLATE utf8_bin DEFAULT NULL,
  `ville` varchar(40) COLLATE utf8_bin DEFAULT NULL,
  `adrMel` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `telephone` char(10) COLLATE utf8_bin DEFAULT NULL,
  `contact` varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `codePays` char(4) COLLATE utf8_bin NOT NULL,
  `mdp` char(10) COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`code`),
  KEY `fk_perspays` (`codePays`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=8 ;

--
-- Contenu de la table `personne`
--

INSERT INTO `personne` (`code`, `raisonSociale`, `adresse`, `cp`, `ville`, `adrMel`, `telephone`, `contact`, `codePays`, `mdp`) VALUES
(1, 'action contre la fin', 'Place d''''Armes', '78000', 'Versailles', 'acfversailles@example.com', '0123456789', 'Louis XIV', 'FRA', '123456'),
(2, 'jules Ferry', '7 Rue Joseph Bouyssel', '78700', 'Conflans Sainte-Honorine', 'lyceejf@gmail.com', '0123456789', 'Morel', 'FRA', 'julesFerry'),
(3, 'Anouk ZOUAOUI', '5 Rue du Pressoir', '78570', 'Chanteloup les Vignes', 'anzouaoui78@yahoo.fr', '0123456789', 'Anouk', 'FRA', 'anzouaoui');

-- --------------------------------------------------------

--
-- Structure de la table `reservation`
--

CREATE TABLE IF NOT EXISTS `reservation` (
  `codeReservation` int(11) NOT NULL AUTO_INCREMENT,
  `dateDebutReservation` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `dateFinReservation` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `dateReservation` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `volumeEstime` decimal(8,0) DEFAULT NULL,
  `codeDevis` smallint(5) DEFAULT NULL,
  `codeVilleMiseDispo` char(3) COLLATE utf8_bin NOT NULL,
  `codeVilleRendre` char(3) COLLATE utf8_bin DEFAULT NULL,
  `code` smallint(8) DEFAULT NULL,
  PRIMARY KEY (`codeReservation`),
  KEY `fk_villeD` (`codeVilleMiseDispo`),
  KEY `fk_villeR` (`codeVilleRendre`),
  KEY `fk_devis` (`codeDevis`),
  KEY `fk_pers` (`code`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=40 ;


-- --------------------------------------------------------

--
-- Structure de la table `reserver`
--

CREATE TABLE IF NOT EXISTS `reserver` (
  `codeReservation` int(11) NOT NULL AUTO_INCREMENT,
  `typeContainer` char(4) COLLATE utf8_bin NOT NULL,
  `qteReserver` int(11) NOT NULL,
  PRIMARY KEY (`codeReservation`,`typeContainer`),
  KEY `fk_codtyp` (`typeContainer`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=40 ;


-- --------------------------------------------------------

--
-- Structure de la table `typecontainer`
--

CREATE TABLE IF NOT EXISTS `typecontainer` (
  `typeContainer` char(4) COLLATE utf8_bin NOT NULL DEFAULT '',
  `libelleTypeContainer` varchar(30) COLLATE utf8_bin NOT NULL,
  `longueurCont` decimal(5,0) NOT NULL,
  `largeurCont` decimal(5,0) NOT NULL,
  `hauteurCont` decimal(4,0) NOT NULL,
  `poidsCont` decimal(5,0) DEFAULT NULL,
  `tare` decimal(4,0) DEFAULT NULL,
  `capaciteDeCharge` decimal(5,2) DEFAULT NULL,
  `prix` decimal(7,2) NOT NULL,
  PRIMARY KEY (`typeContainer`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Contenu de la table `typecontainer`
--

INSERT INTO `typecontainer` (`typeContainer`, `libelleTypeContainer`, `longueurCont`, `largeurCont`, `hauteurCont`, `poidsCont`, `tare`, `capaciteDeCharge`, `prix`) VALUES
('CIT1', 'Citerne 20 pieds', '6', '2', '2', '24000', '4260', '20.00', '15000.00'),
('DRY1', 'Dry 20 pieds', '5', '2', '2', '30', '2230', '33.00', '12000.00'),
('DRY2', 'Dry 40 pieds', '12', '2', '2', '30', '3740', '67.00', '20000.00'),
('FLR1', 'Flat Rack 20 pieds', '5', '2', '2', '34000', '2750', '28.00', '15000.00'),
('FLR2', 'Flat Rack 40 îeds', '11', '1', '2', '34000', '5100', '34.00', '30000.00'),
('HCP1', 'High Cube PalletWide 40 pieds', '12', '2', '2', '34000', '4260', '76.00', '13000.00'),
('HCP2', 'High Cube PalletWide 45 pieds', '13', '2', '2', '34000', '4890', '85.00', '17000.00'),
('OPT1', 'OpenTop 20 pieds', '5', '2', '2', '30', '2200', '32.00', '16000.00'),
('OPT2', 'OpenTop 40 pieds', '12', '2', '3', '30480', '3880', '66.00', '25000.00'),
('REE1', 'Reefer 20 pieds', '5', '2', '2', '30', '3010', '28.00', '14000.00'),
('REE2', 'Reefer 40 pieds', '11', '2', '2', '34000', '4740', '68.00', '18000.00');

-- --------------------------------------------------------

--
-- Structure de la table `ville`
--

CREATE TABLE IF NOT EXISTS `ville` (
  `codeVille` char(3) COLLATE utf8_bin NOT NULL DEFAULT '',
  `nomVille` varchar(30) COLLATE utf8_bin NOT NULL,
  `codePays` char(4) COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`codeVille`),
  KEY `fk_pays` (`codePays`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Contenu de la table `ville`
--

INSERT INTO `ville` (`codeVille`, `nomVille`, `codePays`) VALUES
('ANV', 'Anvers', 'BEL'),
('HAM', 'Hambourg', 'ALL'),
('HAV', 'Havre', 'FRA'),
('MAR', 'Marseille', 'FRA'),
('ROT', 'Rotterdam', 'PB');

--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `personne`
--
ALTER TABLE `personne`
  ADD CONSTRAINT `fk_perspays` FOREIGN KEY (`codePays`) REFERENCES `pays` (`codePays`);

--
-- Contraintes pour la table `reservation`
--
ALTER TABLE `reservation`
  ADD CONSTRAINT `fk_devis` FOREIGN KEY (`codeDevis`) REFERENCES `devis` (`codeDevis`),
  ADD CONSTRAINT `fk_pers` FOREIGN KEY (`code`) REFERENCES `personne` (`code`),
  ADD CONSTRAINT `fk_villeD` FOREIGN KEY (`codeVilleMiseDispo`) REFERENCES `ville` (`codeVille`),
  ADD CONSTRAINT `fk_villeR` FOREIGN KEY (`codeVilleRendre`) REFERENCES `ville` (`codeVille`);

--
-- Contraintes pour la table `reserver`
--
ALTER TABLE `reserver`
  ADD CONSTRAINT `fk_codtyp` FOREIGN KEY (`typeContainer`) REFERENCES `typecontainer` (`typeContainer`),
  ADD CONSTRAINT `reserver_ibfk_1` FOREIGN KEY (`codeReservation`) REFERENCES `reservation` (`codeReservation`);

--
-- Contraintes pour la table `ville`
--
ALTER TABLE `ville`
  ADD CONSTRAINT `fk_pays` FOREIGN KEY (`codePays`) REFERENCES `pays` (`codePays`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
